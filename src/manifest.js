export default {
    spritesheets: [
        { "name": "bunny", "url": "../resources/sprites/test.json"}
    ],
    sprites: [
        { "name": "player", "url": "../resources/sprites/player.png" },
        { "name": "bullet", "url": "../resources/sprites/bullet.png" },
        { "name": "wall", "url": "../resources/sprites/wall.png" },
        { "name": "bg", "url": "../resources/sprites/bg.jpg" }
    ],
    animations: {
        "player": {
            "frames": [[0, 0, 128, 128]],
            "animations": {
                "play": [0, 2],
                "stand": [0]
            }
        },
        "bullet": {
            "frames": [[0, 0, 32, 32], [0, 32, 32, 32], [0, 64, 32, 32], [0, 96, 32, 32], [0, 128, 32, 32], [0, 160, 32, 32], [0, 192, 32, 32], [0, 224, 32, 32], [0, 256, 32, 32], [0, 288, 32, 32], [0, 32, 32, 32], [0, 320, 32, 32], [0, 352, 32, 32], [0, 384, 32, 32], [0, 416, 32, 32], [0, 448, 32, 32]],
            "animations": {
                "play": [0, 2],
                "stand": [0]
            }
        },
        "wall": {
            "frames": [[0, 0, 32, 32]],
            "animations": {
                "play": [0, 2],
                "stand": [0]
            }
        },
        "bg": {
            "frames": [[0, 0, 200, 200]],
            "animations": {
                "play": [0, 2],
                "stand": [0]
            }
        }
    }
};