export const getDistance = (point1, point2) => {
    let a = point1.x - point2.x;
    let b = point1.y - point2.y;
    return Math.sqrt(a * a + b * b);
};