
class Camera {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    };
    setTarget(target) {
        this.target = target;
    }
    update(delta) {
        if (!this.target) return;
        this.x = this.target.x - window.innerWidth / 2;
        this.y = this.target.y - window.innerHeight / 2;
        console.log(this);
    }
}

export default new Camera(500, 500);