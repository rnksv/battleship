import * as PIXI from "pixi.js";
import manifest from '../manifest.js';
import Player from  './objects/Player';
import Wall from  './objects/Wall';
import Camera from './common/Camera';
import Store from './common/Store';

Store.state.objects = [];
Store.state.player = null;


class Game {
    constructor() {
        console.log('Создан объект игры');
        this.app = Store.app;
        this.manifest = manifest;
        this.loader = PIXI.loader;
        this.loader.onProgress.add((loader) => console.log('Загружено: ', loader.progress));
        this.resources = [];
        this.ticker = null;
    };
    init() {
        return new Promise((resolve, reject) => {
            console.log('Игра проинициилизрована');
            document.body.appendChild(this.app.view);
            resolve(this);
        });
    }
    load() {
        console.log('Загрузка ресурсов');
        return new Promise((resolve, reject) => {
            this.manifest.sprites.forEach((spriteData) => {
                console.log('Проверка ресурса', spriteData);
                this.loader.add(spriteData['name'], spriteData['url'])
            });
            this.manifest.spritesheets.forEach((spriteData) => {
                console.log('Проверка ресурса анимации', spriteData);
                this.loader.add(spriteData['name'], spriteData['url'])
            });
            this.loader.load((loader, resources) => {
                console.log('Все ресурсы успешно загружены');
                Store.setResources(resources);
                this.resources = Store.resources;
                resolve(this);
            })
        });
    }
    start() {
        return new Promise((resolve, reject) => {
            console.log('Начинаем игру', this.resources, this.app);
            this.app.ticker.add(delta => {
                Store.state.objects.forEach(object => {
                    object.update(delta);
                });
                Store.state.destructibleObjects.forEach(object => {
                    object.update(delta);
                });
                Camera.update(delta);
            });
            this.app.ticker.speed = 1;
            resolve(this);
        });
    }
    spawnPlayer(id) {
        let player = new Player({
            texture: this.resources['player'].texture,
            animation: this.manifest.animations['player'],
            width: 128,
            height: 128,
            x: 120,
            y: 120
        });
        Store.addObject(id, player);

        player = Store.getObject(id);

        Camera.setTarget(player);
    }
    events() {
        console.log('Запускаем отслеживание игровых событий');

        const map = [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0],
            [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0],
            [1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0],
            [1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0],
            [1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        for (let row in map) {
            for (let col in map[row]) {
                if (map[row][col] === 1) {
                    const wall = new Wall({
                        texture: this.resources['wall'].texture,
                        animation: this.manifest.animations['wall'],
                        width: 64,
                        height: 64,
                        x: col * 64,
                        y: row * 64
                    });
                    Store.addObject('wall1' + row + "1" + col, wall);
                } else {
                    const wall = new Wall({
                        texture: this.resources['bg'].texture,
                        animation: this.manifest.animations['bg'],
                        width: 64,
                        height: 64,
                        x: col * 64,
                        y: row * 64
                    });
                    Store.addObject('wall1' + row + "1" + col + 'c', wall);
                }
            }
        }
        this.spawnPlayer('player1');
        // this.spawnPlayer('player2');
        // setInterval(() => this.spawnPlayer('player' + Math.random()), 1000)
    }
}

export default Game;