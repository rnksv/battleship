import AnimationSprite from '../common/AnimationSprite';
import Store from '../common/Store';
import Camera from '../common/Camera';

import * as PIXI from "pixi.js";
import * as helpers from "../../helpers";

class Wall extends AnimationSprite {
    constructor(params) {
        super(params);
        const healthStyle = {
            fontFamily : 'Arial',
            fontSize: 24,
            fill : 0xff1010,
            align : 'center',
        };

        this.spritesheet.interactive = true;
        this.spritesheet.cursor = 'pointer';
        this.health = {
            count: 100,
            render: new PIXI.Text(100, healthStyle),
        };
        this.type = 'destructible';
        this.addHp();
    }
    addHp() {
        this.health.render.position.x = this.spritesheet.x + this.width / 2;
        this.health.render.position.y = this.spritesheet.y + this.height / 2;
        this.health.render.anchor.set(0.5, 0.5);

        Store.app.stage.addChild(this.health.render);
    }
    update(delta) {
        super.update(delta);

        let point1 = {
            x: this.spritesheet.x,
            y: this.spritesheet.y
        };
        let point2 = {
            x: Store.getObject('player1').spritesheet.x,
            y: Store.getObject('player1').spritesheet.y,
        };
        if (helpers.getDistance(point1, point2) > 400) {
            this.spritesheet.visible = false;
            this.health.render.visible = false;
            return
        } else {
            this.health.render.visible = true;
            this.spritesheet.visible = true;
        }

        if (this.health.count <= 1) {
            Store.removeObject(this._id);
        }
        this.health.render.position.x = this.spritesheet.x + this.width / 2;
        this.health.render.position.y = this.spritesheet.y + this.height / 2;

        this.health.render.text = this.health.count;
    }
    remove() {
        super.remove();
        Store.app.stage.removeChild(this.health.render);
    }
}

export default Wall;