import AnimationSprite from '../common/AnimationSprite';
import Controller from '../common/Controller';
import Store from '../common/Store';

import Bullet from "./Bullet";

class Player extends AnimationSprite {
    constructor(params) {
        super(params);
        this.controller = new Controller();
        this.spritesheet.interactive = true;
        this.spritesheet.cursor = 'pointer';
        this.dx = 0;
        this.dy = 0;
        this.speed = 2.8;
        document.addEventListener('click', this.fire.bind(this));
    }
    fire() {
        let bullet = new Bullet({
            texture: Store.resources['bullet'].texture,
            animation: Store.manifest.animations['player'],
            width: 3,
            height: 3,
            x: this.x,
            y: this.y,
            angle: this.spritesheet.rotation,
            owner: this
        });
        Store.addObject(Math.random(), bullet);
    }
    update(delta) {
        super.update(delta);

        let angle = Math.atan2(
            this.controller.mousePosition.y - this.spritesheet.y,
            this.controller.mousePosition.x - this.spritesheet.x
        );

        this.dx = this.controller.moveVector.right - this.controller.moveVector.left;
        this.dy = this.controller.moveVector.down - this.controller.moveVector.up;

        this.spritesheet.rotation = angle;
        this.x = this.x + this.dx * this.speed;
        this.y = this.y + this.dy * this.speed;

        this.spritesheet.pivot = {
            x: this.width / this.spritesheet.width + this.spritesheet.width / 2,
            y: this.height / this.spritesheet.height + this.spritesheet.height / 2
        };

    }
}

export default Player;